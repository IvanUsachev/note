package com.example.maximus.note;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Note {


    @PrimaryKey
    int id;
    private String nameNote;
    private String textNote;
    private int day;
    private int month;
    private int year;

    public Note(int id, String nameNote, String textNote, int day, int month, int year) {
        this.id = id;
        this.nameNote = nameNote;
        this.textNote = textNote;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getId() {
        return id;
    }

    public String getNameNote() {
        return nameNote;
    }

    public String getTextNote() {
        return textNote;
    }

    public String getName() {
        return nameNote;
    }

    public String getText() {
        return textNote;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", nameNote='" + nameNote + '\'' +
                ", textNote='" + textNote + '\'' +
                ", day=" + day +
                ", month=" + month +
                ", year=" + year +
                '}';
    }
}
