package com.example.maximus.note.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.maximus.note.Note;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface NoteDao {


    @Insert
    void insertAll(Note... notes);

    @Delete
    void delete(Note name);

    @Query("SELECT * FROM note")
    List<Note> getAllNote();
//
//    @Query("SELECT * FROM note WHERE day > 0")
//    List<Note> getAllUserWith18Year(int day);

    @Query("DELETE FROM note")
    void deleteAll();


}



