package com.example.maximus.note.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.maximus.note.Note;
import com.example.maximus.note.R;
import com.example.maximus.note.activity.MainActivity;
import com.example.maximus.note.dao.LibDataBase;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Note> notes;
    private OnRecyclItemClic onRecyclItemClic;

    public RecyclerViewAdapter(Context context, List<Note> notes, OnRecyclItemClic onRecyclItemClic) {
        this.context = context;
        this.notes = notes;
        this.onRecyclItemClic = onRecyclItemClic;
    }

    public RecyclerViewAdapter(MainActivity context, LibDataBase db, MainActivity onRecyclItemClic) {
    }

    public interface OnRecyclItemClic {
        void onClick(int position);
    }

    public void addItem(Note note) {
        notes.add(1, note);
        notifyItemInserted(1);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.r_list_layout, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Note note = notes.get(position);
        holder.nameNote.setText(note.getName());
        holder.textNote.setText(note.getText().substring(0,20));
        holder.dayCreateNote.setText(String.valueOf(note.getDay()));
        holder.monthCreateNote.setText(String.valueOf(note.getMonth()));
        holder.yearCreateNote.setText(String.valueOf(note.getYear()));

    }

    public void deleteNote(int position) {
        if (position >= 0 && position < notes.size()) {
            notes.remove(position);
            notifyItemRangeRemoved(position, position);
        }
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public Note getItem(int position) {
        return notes.get(position);
    }



    class ViewHolder extends RecyclerView.ViewHolder {
        TextView nameNote, textNote, dayCreateNote, monthCreateNote, yearCreateNote;
        Button deleteNote;

        public ViewHolder(View item) {
            super(item);

            deleteNote = (Button) item.findViewById(R.id.delete_note);
            nameNote = (TextView) item.findViewById(R.id.note_name);
            textNote = (TextView) item.findViewById(R.id.note_text);
            dayCreateNote = (TextView) item.findViewById(R.id.day_create_note);
            monthCreateNote = (TextView) item.findViewById(R.id.month_create_note);
            yearCreateNote = (TextView) item.findViewById(R.id.year_create_note);

            deleteNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteNote(getLayoutPosition());
                }
            });


            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecyclItemClic.onClick(getAdapterPosition());
                }
            });
        }



    }
}
