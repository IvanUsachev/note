package com.example.maximus.note.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maximus.note.R;

public class NoteInfoActivity extends AppCompatActivity {
    TextView nameNote, textNote, dayCreateNote, monthCreateNote, yearCreateNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_info);

        String nameNoteValue, textNoteValue;
        int dayCreateNoteValue, monthCreateNoteValue, yearCreateNoteValue;
        nameNoteValue = getIntent().getStringExtra("name");
        textNoteValue = getIntent().getStringExtra("text");
        dayCreateNoteValue = getIntent().getIntExtra("day", 0);
        monthCreateNoteValue = getIntent().getIntExtra("month", 0);
        yearCreateNoteValue = getIntent().getIntExtra("year", 0);


        nameNote = (TextView) findViewById(R.id.note_name);
        textNote = (TextView) findViewById(R.id.note_text);
        dayCreateNote = (TextView) findViewById(R.id.day_create_note);
        monthCreateNote = (TextView) findViewById(R.id.month_create_note);
        yearCreateNote = (TextView) findViewById(R.id.year_create_note);


        nameNote.setText(nameNoteValue);
        textNote.setText(textNoteValue);
        dayCreateNote.setText(String.valueOf(dayCreateNoteValue));
        monthCreateNote.setText(String.valueOf(monthCreateNoteValue));
        yearCreateNote.setText(String.valueOf(yearCreateNoteValue));


    }
}
