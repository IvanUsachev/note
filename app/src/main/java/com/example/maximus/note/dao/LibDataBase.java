package com.example.maximus.note.dao;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.maximus.note.Note;

@Database(entities = {Note.class}, version = 1)
public abstract class LibDataBase extends RoomDatabase {

    public abstract NoteDao getUserDao();
}


