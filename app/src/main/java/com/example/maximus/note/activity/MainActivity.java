package com.example.maximus.note.activity;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.maximus.note.Note;
import com.example.maximus.note.R;
import com.example.maximus.note.adapter.RecyclerViewAdapter;
import com.example.maximus.note.dao.LibDataBase;
import com.example.maximus.note.generator.Generator;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements RecyclerViewAdapter.OnRecyclItemClic {

    RecyclerViewAdapter adapter;
    Button addNewNote;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addNewNote = (Button) findViewById(R.id.button);

        final LibDataBase db = Room.databaseBuilder(getApplicationContext(),
                LibDataBase.class, "note-database").build();


        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter(this, Generator.generate(), this);
        recyclerView.setAdapter(adapter);

        rx.Observable.just(db)
                .subscribeOn(Schedulers.io())
                .map(new Func1<LibDataBase, List<Note>>() {
                    @Override
                    public List<Note> call(LibDataBase libDataBase) {
                        db.getUserDao().deleteAll();
                        for (int i = 0; i < 100; i++) {
                            db.getUserDao().insertAll(
                                    new Note(i, "Vasi", ";dl", 05, 12, 1986)
                            );
                        }
                        return db.getUserDao().getAllNote();
                    }
                })

                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<Note>>() {
                    @Override
                    public void call(List<Note> notes) {
                        for (Note note : notes) {
                            Log.d("note", note.toString());
                        }
                    }

                });


        addNewNote.setOnClickListener(v -> {
            adapter.addItem(new Note(1, "New Note", "new note", 4, 3, 2001));
            recyclerView.smoothScrollToPosition(0);
        });
    }


    @Override
    public void onClick(int position) {
        Intent intent = new Intent(MainActivity.this, NoteInfoActivity.class);
        intent.putExtra("name", adapter.getItem(position).getName());
        intent.putExtra("text", adapter.getItem(position).getText());
        intent.putExtra("day", adapter.getItem(position).getDay());
        intent.putExtra("month", adapter.getItem(position).getMonth());
        intent.putExtra("year", adapter.getItem(position).getYear());
        startActivity(intent);
    }
}
